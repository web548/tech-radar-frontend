import TechnologyOverviewPage from './overview';
import TechnologyEditPage from './edit';
import TechnologyAddPage from './add';
import TechnologyPublishPage from './publish';

export {
	TechnologyOverviewPage,
	TechnologyEditPage,
	TechnologyAddPage,
	TechnologyPublishPage
};