import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from "react-router-dom";
import { Box, AppBar, Toolbar, IconButton, Typography, FormControl, InputLabel, TextField, Select, MenuItem, Button, Container, FormHelperText, Snackbar, Alert } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import SaveIcon from '@mui/icons-material/Save';
import { Technology, TechnologyCreation, Category, Ring } from '../types';
import { getCategories } from '../api/category';
import { getRings } from '../api/ring';
import { updateTechnology } from '../api/technology';

type LocationState = {
	technology: Technology
}

function TechnologyEditPage() {
	const navigate = useNavigate();
	const location = useLocation();

	const [ formSubmitAttempted, setFormSubmitAttempted ] = useState(false);
	const [ showErrorSnackbar, setShowErrorSnackbar ] = useState(false);

	const [ rings, setRings ] = useState<Ring[]>([]);
	const [ categories, setCategories ] = useState<Category[]>([]);

	const [ apiDataLoaded, setApiDataLoaded ] = useState(false);

	// deep copy, so original state doesn't change
	const [ technology, setTechnology ] = useState<Technology>(JSON.parse(JSON.stringify((location.state as LocationState).technology)));

	const isFormValid = () => (technology.name && technology.categoryId && technology.description);

	useEffect(() => {
		Promise.all([getCategories(), getRings()]).then(data => {
			const [ allCategories, allRings ] = data;
			setCategories(allCategories);
			setRings(allRings);

			setApiDataLoaded(true);
		});
	}, []);

	const handleBackClick = () => {
		navigate(-1);
	}

	const handleErrorSnackbarClose = () => {
		setShowErrorSnackbar(false);
	}

	const handleSaveClick = () => {
		setFormSubmitAttempted(true);

		if (!isFormValid())	return;

		const newTechnology: TechnologyCreation = {
			name: technology.name,
			categoryId: technology.categoryId,
			description: technology.description
		}

		updateTechnology(newTechnology, technology.id).then(() => {
			// success
			navigate("/technology", { replace: true });
		}).catch(e => {
			setShowErrorSnackbar(true);
		});
	}

	return (
		<React.Fragment>
			<AppBar position="static">
				<Toolbar>
					<IconButton
						size="large"
						edge="start"
						aria-label="back"
						sx={{ mr: 2 }}
						onClick={handleBackClick}
					>
						<ArrowBackIcon />
					</IconButton>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						Edit Technology
					</Typography>
				</Toolbar>
			</AppBar>

			<Container>
				<Box
					component="form"
					sx={{ display: 'flex', flexWrap: 'wrap' }}
					noValidate
					autoComplete="off"
				>

					<FormControl fullWidth sx={{ m: 1 }}>
						<TextField 
							error={formSubmitAttempted && !technology.name} 
							required 
							id="name" 
							label="Name" 
							variant="standard" 
							value={technology.name} 
							onChange={(e) => setTechnology(tech => ({...tech, name: e.target.value}))} 
							helperText={formSubmitAttempted && !technology.name && 'Please provide the technology name'}
						/>
					</FormControl>

					<FormControl fullWidth variant="standard" sx={{ m: 1 }} error={formSubmitAttempted && !technology.categoryId} required>
						<InputLabel id="category-label">Category</InputLabel>
						<Select
							labelId="category-label"
							id="category"
							label="Category"
							value={apiDataLoaded ? technology.categoryId : ''}
							onChange={(e) => setTechnology(tech => ({...tech, categoryId: e.target.value as number}))}
						>
						{categories.map(category => (
							<MenuItem key={category.id} value={category.id}>{category.name}</MenuItem>
						))}
						</Select>
						<FormHelperText>{formSubmitAttempted && !technology.categoryId && 'Please categorize the technology'}</FormHelperText>
					</FormControl>

					<FormControl fullWidth variant="standard" sx={{ m: 1 }} disabled>
						<InputLabel id="ring-label">Ring</InputLabel>
						<Select
							labelId="ring-label"
							id="ring"
							label="Ring"
							value={apiDataLoaded ? (technology.ringId || '') : ''}
							onChange={(e) => setTechnology(tech => ({...tech, ringId: e.target.value as number}))}
						>
						<MenuItem key={0} value={''}>None</MenuItem>
						{rings.map(ring => (
							<MenuItem key={ring.id} value={ring.id}>{ring.name}</MenuItem>
						))}
						</Select>
					</FormControl>

					<FormControl fullWidth sx={{ m: 1 }}>
						<TextField 
							error={formSubmitAttempted && !technology.description} 
							required 
							multiline 
							id="description" 
							label="Description" 
							variant="standard" 
							value={technology.description} 
							onChange={(e) => setTechnology(tech => ({...tech, description: e.target.value}))} 
							helperText={formSubmitAttempted && !technology.description && 'Please describe the technology'}
						/>
					</FormControl>

					<FormControl fullWidth sx={{ m: 1 }}>
						<TextField 
							disabled
							multiline 
							id="ringPlacementDescription" 
							label="Categorisation description" 
							variant="standard" 
							value={technology.ringPlacementDescription} 
							onChange={(e) => setTechnology(tech => ({...tech, ringPlacementDescription: e.target.value}))} 
						/>
					</FormControl>
				</Box>

				<Box sx={{ m: 1 }}>
					<Button 
						variant="outlined" 
						startIcon={<SaveIcon />} 
						onClick={handleSaveClick}
						disabled={formSubmitAttempted && !isFormValid()}
					>
						Save
					</Button>
				</Box>

				<Snackbar open={showErrorSnackbar} autoHideDuration={5000} onClose={handleErrorSnackbarClose}>
					<Alert severity="error" sx={{ width: '100%' }}>
						And error ocurred, please try again later.
					</Alert>
				</Snackbar>
			</Container>
		</React.Fragment>
	);
}

export default TechnologyEditPage;