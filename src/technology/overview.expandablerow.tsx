import React, { useState } from 'react';
import { TechnologyExpanded } from '../types';
import { Collapse, Box, IconButton, List, ListItem, Grid, Divider } from '@mui/material';
import { TableRow, TableCell } from '@mui/material';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';


function ExpandableRow(props: { technology: TechnologyExpanded, children?: JSX.Element[] }) {
	const { technology } = props;
	const [ open, setOpen ] = useState(false);

	return (
		<React.Fragment>
			<TableRow sx={{ '& > *': { borderBottom: 'unset!important' } }} hover key={technology.id}>
				<TableCell sx={{ display: {xs: 'table-cell', sm: 'none'} }}>
					<IconButton
							aria-label="expand row"
							size="small"
							onClick={() => setOpen(!open)}
						>
							{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
						</IconButton>
				</TableCell>
				<TableCell>{technology.name}</TableCell>
				<TableCell sx={{ display: {xs: 'none', md: 'table-cell'} }}>{technology.category?.name}</TableCell>
				<TableCell sx={{ display: {xs: 'none', md: 'table-cell'} }}>{technology.ring?.name}</TableCell>
				<TableCell>{technology.description}</TableCell>
				<TableCell sx={{ display: {xs: 'none', sm: 'table-cell'} }} align="center">
					{technology.published ? <CheckIcon color="success" /> : <CloseIcon color="error" />}
				</TableCell>
				<TableCell sx={{ display: {xs: 'none', sm: 'table-cell'} }} align="right">
					{props.children}
				</TableCell>
			</TableRow>

			<TableRow sx={{ display: {xs: 'table-row', sm: 'none'} }}>
				<TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={3}>
					<Collapse in={open} timeout="auto" unmountOnExit>
						<Box>
							<Divider />
							<List dense>
									<ListItem>
										<Grid container spacing={2}>
											<Grid item xs={4}>
												Category:
											</Grid>
											<Grid item xs={8}>
												{technology.category?.name}
											</Grid>
										</Grid>
									</ListItem>
									<ListItem>
										<Grid container spacing={2}>
											<Grid item xs={4}>
												Ring:
											</Grid>
											<Grid item xs={8}>
												{technology.ring?.name}
											</Grid>
										</Grid>
									</ListItem>
									<ListItem>
										<Grid container spacing={2}>
											<Grid item xs={4}>
												Published:
											</Grid>
											<Grid item xs={8}>
												{technology.published ? <CheckIcon color="success" /> : <CloseIcon color="error" />}
											</Grid>
										</Grid>
									</ListItem>
									<ListItem>
										<Grid container spacing={2}>
											<Grid item xs={4}>
												Actions:
											</Grid>
											<Grid item xs={8}>
												{props.children}
											</Grid>
										</Grid>
									</ListItem>
							</List>
						</Box>
					</Collapse>
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
}

export default ExpandableRow;