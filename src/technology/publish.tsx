import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from "react-router-dom";
import { Box, AppBar, Toolbar, IconButton, Typography, FormControl, InputLabel, TextField, Select, MenuItem, Button, Container, FormHelperText, Snackbar, Alert } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import PublishIcon from '@mui/icons-material/Publish';
import { Technology, TechnologyPublication, Category, Ring } from '../types';
import { getCategories } from '../api/category';
import { getRings } from '../api/ring';
import { updateTechnology, publishTechnology } from '../api/technology';

type LocationState = {
    technology: Technology
}

function TechnologyPublishPage() {
    const navigate = useNavigate();
    const location = useLocation();

    const [formSubmitAttempted, setFormSubmitAttempted] = useState(false);
    const [showErrorSnackbar, setShowErrorSnackbar] = useState(false);

    const [rings, setRings] = useState < Ring[] > ([]);
    const [categories, setCategories] = useState < Category[] > ([]);

    const [apiDataLoaded, setApiDataLoaded] = useState(false);

    // deep copy, so original state doesn't change
    const [technology, setTechnology] = useState < Technology > (JSON.parse(JSON.stringify((location.state as LocationState).technology)));

    const isFormValid = () => (technology.ringId && technology.ringPlacementDescription);

    useEffect(() => {
        Promise.all([getCategories(), getRings()]).then(data => {
            const [allCategories, allRings] = data;
            setCategories(allCategories);
            setRings(allRings);

            setApiDataLoaded(true);
        });
    }, []);

    const handleBackClick = () => {
        navigate(-1);
    }

    const handleErrorSnackbarClose = () => {
        setShowErrorSnackbar(false);
    }

    const handlePublishClick = () => {
        setFormSubmitAttempted(true);

        if (!isFormValid()) return;

        const newTechnology: TechnologyPublication = {
            ringId: technology.ringId as number,
            ringPlacementDescription: technology.ringPlacementDescription as string
        }

        const apiRequestPromise = technology.published ? updateTechnology(newTechnology, technology.id) : publishTechnology(newTechnology, technology.id);
        apiRequestPromise.then((newTech) => {
            // success
            navigate("/technology", { replace: true });
        }).catch(e => {
            setShowErrorSnackbar(true);
        });
    }

    return (
        <React.Fragment>
			<AppBar position="static">
				<Toolbar>
					<IconButton
						size="large"
						edge="start"
						aria-label="back"
						sx={{ mr: 2 }}
						onClick={handleBackClick}
					>
						<ArrowBackIcon />
					</IconButton>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						Publish Technology
					</Typography>
				</Toolbar>
			</AppBar>

			<Container>
				<Box
					component="form"
					sx={{ display: 'flex', flexWrap: 'wrap' }}
					noValidate
					autoComplete="off"
				>

					<FormControl fullWidth sx={{ m: 1 }}>
						<TextField 
							disabled 
							id="name" 
							label="Name" 
							variant="standard" 
							value={technology.name} 
						/>
					</FormControl>

					<FormControl fullWidth variant="standard" sx={{ m: 1 }} disabled>
						<InputLabel id="category-label">Category</InputLabel>
						<Select
							labelId="category-label"
							id="category"
							label="Category"
							value={apiDataLoaded ? technology.categoryId : ''}
						>
						{categories.map(category => (
							<MenuItem key={category.id} value={category.id}>{category.name}</MenuItem>
						))}
						</Select>
					</FormControl>

					<FormControl fullWidth variant="standard" sx={{ m: 1 }} error={formSubmitAttempted && !technology.ringId} required>
						<InputLabel id="ring-label">Ring</InputLabel>
						<Select
							labelId="ring-label"
							id="ring"
							label="Ring"
							value={apiDataLoaded ? (technology.ringId || '') : ''}
							onChange={(e) => setTechnology(tech => ({...tech, ringId: e.target.value as number}))}
						>
						<MenuItem key={0} value={''}>None</MenuItem>
						{rings.map(ring => (
							<MenuItem key={ring.id} value={ring.id}>{ring.name}</MenuItem>
						))}
						</Select>
						<FormHelperText>{formSubmitAttempted && !technology.ringId && 'Please assign the technology to a ring'}</FormHelperText>
					</FormControl>

					<FormControl fullWidth sx={{ m: 1 }}>
						<TextField 
							disabled 
							multiline 
							id="description" 
							label="Description" 
							variant="standard" 
							value={technology.description}
						/>
					</FormControl>

					<FormControl fullWidth sx={{ m: 1 }}>
						<TextField 
							required
							error={formSubmitAttempted && !technology.ringPlacementDescription} 
							multiline 
							id="ringPlacementDescription" 
							label="Categorisation description" 
							variant="standard" 
							value={technology.ringPlacementDescription} 
							onChange={(e) => setTechnology(tech => ({...tech, ringPlacementDescription: e.target.value}))} 
							helperText={formSubmitAttempted && !technology.ringPlacementDescription && 'Please describe the placement'}
						/>
					</FormControl>
				</Box>

				<Box sx={{ m: 1 }}>
					<Button 
							variant="outlined" 
							startIcon={<PublishIcon />} 
							onClick={handlePublishClick}
							disabled={formSubmitAttempted && !isFormValid()}
						>
							{ technology.published ? 'Update categorisation' : 'Publish' }
						</Button>
				</Box>

				<Snackbar open={showErrorSnackbar} autoHideDuration={5000} onClose={handleErrorSnackbarClose}>
					<Alert severity="error" sx={{ width: '100%' }}>
						And error ocurred, please try again later.
					</Alert>
				</Snackbar>
			</Container>
		</React.Fragment>
    );
}

export default TechnologyPublishPage;