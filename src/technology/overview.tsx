import React, { useState, useEffect, useRef } from 'react';
import { useNavigate } from "react-router-dom";
import { Technology, TechnologyExpanded } from '../types';
import ExpandableRow from './overview.expandablerow';
import { Paper, IconButton, AppBar, Toolbar, Typography, Button } from '@mui/material';
import { TableContainer, Table, TableBody, TableHead, TableRow, TableCell } from '@mui/material';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import PublishIcon from '@mui/icons-material/Publish';

import { getTechnologies, deleteTechnology } from '../api/technology';

function TechnologyOverviewPage() {
	const navigate = useNavigate();
	const technologyToDelete = useRef<number>();

	const [ technologies, setTechnologies ] = useState<TechnologyExpanded[]>([]);
	const [ deleteDialogOpen, setDeleteDialogOpen ] = useState(false);

	useEffect(() => {
		getTechnologies({
			expand: {
				ring: true,
				category: true
			}
		}).then((newTechnologies) => {
			setTechnologies(newTechnologies as TechnologyExpanded[]);
		});
	}, []);

	const handleAddClick = () => {
		navigate("/technology/add", { replace: false });
	}

	const handleEditClick = (technology: Technology) => {
		navigate("/technology/edit", { state: { technology }, replace: false });
	}

	const handlePublishClick = (technology: Technology) => {
		navigate("/technology/publish", { state: { technology }, replace: false });
	}	

	const handleDeleteDialogOpen = () => {
		setDeleteDialogOpen(true);
	}

	const handleDeleteDialogClose = () => {
		setDeleteDialogOpen(false);
	}

	const handleDeleteClick = (id: number) => {
		technologyToDelete.current = id;
		handleDeleteDialogOpen();
	}

	const handleDeleteConfirmation = () => {
		const technologyId = technologyToDelete.current as number;

		deleteTechnology(technologyId).then(() => {
			// success
			setTechnologies(newTechnologies => newTechnologies.filter(tech => tech.id !== technologyId));
			handleDeleteDialogClose();
		}).catch(handleDeleteDialogClose);
	}

	return (
		<React.Fragment>
			<AppBar position="static">
				<Toolbar>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						Technologies
					</Typography>
					<IconButton
						size="large"
						edge="end"
						aria-label="add"
						sx={{ mr: 2 }}
						onClick={handleAddClick}
					>
						<AddIcon />
					</IconButton>
				</Toolbar>
			</AppBar>
			<TableContainer component={Paper}>
				<Table stickyHeader sx={{ minWidth: 320 }} aria-label="technology table">
					<TableHead>
						<TableRow>
							<TableCell sx={{ display: {xs: 'table-cell', sm: 'none'} }}></TableCell>
							<TableCell>Technology</TableCell>
							<TableCell sx={{ display: {xs: 'none', md: 'table-cell'} }}>Category</TableCell>
							<TableCell sx={{ display: {xs: 'none', md: 'table-cell'} }}>Ring</TableCell>
							<TableCell>Description</TableCell>
							<TableCell sx={{ display: {xs: 'none', sm: 'table-cell'} }} align="center">Published</TableCell>
							<TableCell sx={{ display: {xs: 'none', sm: 'table-cell'} }} align="right">Actions</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{technologies.map((technology) => (
							<ExpandableRow key={technology.id} technology={technology}>
								<IconButton aria-label="edit" color="primary" size="small" onClick={() => handleEditClick(technology)}>
									<EditIcon />
								</IconButton>

								<IconButton aria-label="publish" color="success" size="small" onClick={() => handlePublishClick(technology)}>
									<PublishIcon />
								</IconButton>

								<IconButton aria-label="delete" color="error" size="small" onClick={() => handleDeleteClick(technology.id)}>
									<DeleteIcon />
								</IconButton>
							</ExpandableRow>
						))}
					</TableBody>
				</Table>
				<Dialog
					open={deleteDialogOpen}
					onClose={() => handleDeleteDialogClose()}
					aria-labelledby="delete"
					aria-describedby="are you sure that you want to delete this technology?"
				>
					<DialogTitle>
						Are you sure that you want to delete this technology?
					</DialogTitle>
					<DialogContent>
						<DialogContentText id="alert-dialog-description">
							Once the technology has been deleted, it cannot be recovered.
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={() => handleDeleteDialogClose()}>Cancel</Button>
						<Button onClick={() => handleDeleteConfirmation()} autoFocus color="error">
							Delete
						</Button>
					</DialogActions>
				</Dialog>
			</TableContainer>
		</React.Fragment>
	);
}

export default TechnologyOverviewPage;
