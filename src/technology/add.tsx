import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { Box, AppBar, Toolbar, IconButton, Typography, FormControl, InputLabel, TextField, Select, MenuItem, Button, Container, FormHelperText, Snackbar, Alert } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import SaveIcon from '@mui/icons-material/Save';
import { TechnologyCreation, Category, Ring } from '../types';
import { getCategories } from '../api/category';
import { getRings } from '../api/ring';
import { createTechnology } from '../api/technology';

function TechnologyAddPage() {
	const navigate = useNavigate();

	const [ name, setName ] = useState('');
	const [ category, setCategory ] = useState<number | string>('');
	const [ ring, setRing ] = useState<number | string>('');
	const [ description, setDescription ] = useState('');
	const [ ringPlacementDescription, setRingPlacementDescription ] = useState('');

	const [ formSubmitAttempted, setFormSubmitAttempted ] = useState(false);
	const [ showErrorSnackbar, setShowErrorSnackbar ] = useState(false);

	const [ rings, setRings ] = useState<Ring[]>([]);
	const [ categories, setCategories ] = useState<Category[]>([]);

	const isFormValid = () => (name && category && description);

	useEffect(() => {
		getCategories().then((allCategories) => {
			setCategories(allCategories);
			if (allCategories.length > 0){
				setCategory(allCategories[0].id);
			}
		});
		getRings().then((allRings) => {
			setRings(allRings);
		});
	}, []);

	const handleBackClick = () => {
		navigate(-1);
	}

	const handleErrorSnackbarClose = () => {
		setShowErrorSnackbar(false);
	}

	const handleSaveClick = () => {
		setFormSubmitAttempted(true);

		if (!isFormValid())	return;

		const technology: TechnologyCreation = {
			name,
			categoryId: category as number,
			ringId: ring as number || null,
			description,
			ringPlacementDescription
		};

		createTechnology(technology).then(() => {
			// success
			navigate("/technology", { replace: true });
		}).catch(e => {
			setShowErrorSnackbar(true);
		});
	}

	return (
		<React.Fragment>
			<AppBar position="static">
				<Toolbar>
					<IconButton
						size="large"
						edge="start"
						aria-label="back"
						sx={{ mr: 2 }}
						onClick={handleBackClick}
					>
						<ArrowBackIcon />
					</IconButton>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						Add Technology
					</Typography>
				</Toolbar>
			</AppBar>

			<Container>
				<Box
					component="form"
					sx={{ display: 'flex', flexWrap: 'wrap' }}
					noValidate
					autoComplete="off"
				>

					<FormControl fullWidth sx={{ m: 1 }}>
						<TextField 
							error={formSubmitAttempted && !name} 
							required 
							id="name" 
							label="Name" 
							variant="standard" 
							value={name} 
							onChange={(e) => setName(e.target.value)} 
							helperText={formSubmitAttempted && !name && 'Please provide the technology name'}
						/>
					</FormControl>

					<FormControl fullWidth variant="standard" sx={{ m: 1 }} error={formSubmitAttempted && !category}>
						<InputLabel id="category-label">Category</InputLabel>
						<Select
							required
							labelId="category-label"
							id="category"
							label="Category"
							value={category}
							onChange={(e) => setCategory(e.target.value as number)}
						>
						{categories.map(category => (
							<MenuItem key={category.id} value={category.id}>{category.name}</MenuItem>
						))}
						</Select>
						<FormHelperText>{formSubmitAttempted && !category && 'Please categorize the technology'}</FormHelperText>
					</FormControl>

					<FormControl fullWidth variant="standard" sx={{ m: 1 }}>
						<InputLabel id="ring-label">Ring</InputLabel>
						<Select
							labelId="ring-label"
							id="ring"
							label="Ring"
							value={ring}
							onChange={(e) => setRing(e.target.value as number)}
						>
						<MenuItem key={0} value={''}>None</MenuItem>
						{rings.map(ring => (
							<MenuItem key={ring.id} value={ring.id}>{ring.name}</MenuItem>
						))}
						</Select>
					</FormControl>

					<FormControl fullWidth sx={{ m: 1 }}>
						<TextField 
							error={formSubmitAttempted && !description} 
							required 
							multiline 
							id="description" 
							label="Description" 
							variant="standard" 
							value={description} 
							onChange={(e) => setDescription(e.target.value)} 
							helperText={formSubmitAttempted && !description && 'Please describe the technology'}
						/>
					</FormControl>

					<FormControl fullWidth sx={{ m: 1 }}>
						<TextField 
							multiline 
							id="ringPlacementDescription" 
							label="Categorisation description" 
							variant="standard" 
							value={ringPlacementDescription} 
							onChange={(e) => setRingPlacementDescription(e.target.value)} 
						/>
					</FormControl>
				</Box>

				<Box sx={{ m: 1 }}>
					<Button 
						variant="outlined" 
						startIcon={<SaveIcon />} 
						onClick={handleSaveClick}
						disabled={formSubmitAttempted && !isFormValid()}
					>
						Save
					</Button>
				</Box>

				<Snackbar open={showErrorSnackbar} autoHideDuration={5000} onClose={handleErrorSnackbarClose}>
					<Alert severity="error" sx={{ width: '100%' }}>
						And error ocurred, please try again later.
					</Alert>
				</Snackbar>
			</Container>
		</React.Fragment>
	);
}

export default TechnologyAddPage;