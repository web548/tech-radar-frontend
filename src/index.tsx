import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { TechnologyOverviewPage, TechnologyEditPage, TechnologyAddPage, TechnologyPublishPage } from './technology';
import { TechnologyViewerPage, TechnologyViewerHistoryPage } from './viewer';

import App from './App';
import reportWebVitals from './reportWebVitals';

const rootElement = document.getElementById("root");
render(
	<React.StrictMode>
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<Navigate to="/viewer" />} />
				<Route path="/" element={<App />} >
					<Route path="viewer" element={<TechnologyViewerPage />} />
					<Route path="viewer/history" element={<TechnologyViewerHistoryPage />} />
					<Route path="technology/" element={<TechnologyOverviewPage />} />
					<Route path="technology/edit" element={<TechnologyEditPage />} />
					<Route path="technology/add" element={<TechnologyAddPage />} />
					<Route path="technology/publish" element={<TechnologyPublishPage />} />
				</Route>
			</Routes>
		</BrowserRouter>
	</React.StrictMode>,
	rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
