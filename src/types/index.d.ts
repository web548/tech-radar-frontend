interface Ring {
	id: number,
	name: string
}

interface Category {
	id: number,
	name: string
}

interface Technology {
	id: number,
	categoryId: number,
	ringId: number?,
	name: string
	description: string
	ringPlacementDescription: string?,
	createdAt: Date,
	modifiedAt: Date,
	published: boolean,
	publishedAt: Date?
}

interface TechnologyExpanded extends Technology {
	ring: Ring?,
	category: Category?
}

interface TechnologyCreation {
	id?: number,
	categoryId: number,
	ringId?: number?,
	name: string
	description: string
	ringPlacementDescription?: string?,
	createdAt?: Date,
	modifiedAt?: Date,
	published?: boolean,
	publishedAt?: Date
}

interface TechnologyPublication {
	id?: number,
	ringId: number,
	ringPlacementDescription: string
}

interface RingPlacementHistory {
	id: number,
	technologyId: number,
	newRingId: number,
	oldRingId: number,
	newRingPlacementDescription: string,
	oldRingPlacementDescription: string,
	modifiedAt: Date
}

interface RingPlacementHistoryExpanded extends RingPlacementHistory{
	newRing: Ring,
	oldRing: Ring
}

export { Ring, Category, Technology, TechnologyExpanded, TechnologyCreation, TechnologyPublication, RingPlacementHistory, RingPlacementHistoryExpanded };