import React, { useEffect, useState, useMemo } from 'react';
import { Link, Outlet, useLocation } from "react-router-dom";
import { BottomNavigation, BottomNavigationAction, Paper, CssBaseline, Box } from '@mui/material';
import ViewListIcon from '@mui/icons-material/ViewList';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import useMediaQuery from '@mui/material/useMediaQuery';
import { createTheme, ThemeProvider } from '@mui/material/styles';
	
function App() {
	// define theme dynamically depending on user preferrence
	const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
	const theme = useMemo(
		() =>
			createTheme({
				palette: {
					mode: prefersDarkMode ? 'dark' : 'light',
				},
			}),
		[prefersDarkMode],
	);

	const location = useLocation();
	const [ activeNavigationItem, setActiveNavigationItem ] = useState('viewer');

	useEffect(() => {
		// quickfix
		setActiveNavigationItem(location.pathname.includes('technology') ? 'technology' : 'viewer');
	}, []);

	return (
		<ThemeProvider theme={theme}>
			<Box sx={{ pb: 7 }}>
				<CssBaseline />
				<Outlet />
				<Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={3}>
					<BottomNavigation
						showLabels
						value={activeNavigationItem}
						onChange={(event, newValue) => {
							setActiveNavigationItem(newValue);
						}}
					>
						<BottomNavigationAction component={Link} to="/" label="Viewer" value="viewer" icon={<ViewListIcon />} />
						<BottomNavigationAction component={Link} to="/technology" label="Editor" value="technology" icon={<ModeEditIcon />} />
					</BottomNavigation>
				</Paper>
			</Box>
	    </ThemeProvider>
	);
}

export default App;
