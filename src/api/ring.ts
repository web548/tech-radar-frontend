import axios from 'axios';
import env from "react-dotenv";
import { Ring } from '../types';

const baseUrl: string = env.API_LINK;

const getRings = () => {
	return new Promise<Ring[]>(async (resolve, reject) => {
		axios.get(baseUrl + "ring").then(response => {
			if (response.status === 200){
				resolve(response.data);
			}else{
				reject(response);
			}
		}).catch(e => {
			reject(e);
		});
	})
}

export { getRings };