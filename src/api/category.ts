import axios from 'axios';
import env from "react-dotenv";
import { Category } from '../types';

const baseUrl: string = env.API_LINK;

const getCategories = () => {
	return new Promise<Category[]>(async (resolve, reject) => {
		axios.get(baseUrl + "category").then(response => {
			if (response.status === 200){
				resolve(response.data);
			}else{
			}
		}).catch(e => {
			reject(e);
		});
	})
}

export { getCategories };