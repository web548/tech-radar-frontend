import axios from 'axios';
import env from "react-dotenv";
import { Technology, TechnologyCreation, TechnologyExpanded, TechnologyPublication, RingPlacementHistory, RingPlacementHistoryExpanded } from '../types';

const baseUrl: string = env.API_LINK;

interface TechnologyExpandType {
	ring?: boolean,
	category?: boolean
}

interface TechnologyRingPlacementExpandType {
	newRing?: boolean,
	oldRing?: boolean
}

const getTechnologies = ( { published, expand }: { published?: boolean, expand?: TechnologyExpandType } = {} ) => {
	return new Promise<Technology[] | TechnologyExpanded[]>(async (resolve, reject) => {
		let firstParam = true;

		let url = `${baseUrl}technology`;

		if (published !== undefined){
			url += `${firstParam ? '?' : '&' }published=${published ? 1 : 0}`;
			firstParam = false;
		}

		if (expand !== undefined){
			const expandString = Object.entries(expand)
														.filter(([key, value]) => value)
														.map(([key, value]) => key)
														.join(',');

			url += `${firstParam ? '?' : '&' }expand=${expandString}`;
			firstParam = false;
		}

		axios.get(url).then(response => {
			if (response.status === 200){
				resolve(response.data);
			}else{
				reject(response);
			}
		}).catch(e => {
			reject(e);
		});
	})
}

const createTechnology = (technology: TechnologyCreation) => {
	return new Promise<Technology>(async (resolve, reject) => {
		axios.post(baseUrl + "technology", technology).then(response => {
			if (response.status === 200){
				resolve(response.data);
			}else{
				reject(response);
			}
		}).catch(e => {
			reject(e);
		});
	});
}

const updateTechnology = (technology: Technology | TechnologyCreation | TechnologyPublication, id?: number) => {
	return new Promise<Technology>(async (resolve, reject) => {
		const technologyId = id || technology.id;

		axios.put(baseUrl + "technology/" + technologyId, technology).then(response => {
			if (response.status === 200){
				resolve(response.data);
			}else{
				reject(response);
			}
		}).catch(e => {
			reject(e);
		});
	});
}

const publishTechnology = (technology: TechnologyPublication, id?: number) => {
	return new Promise<Technology>(async (resolve, reject) => {
		axios.put(`${baseUrl}technology/${id}/publish`, technology).then(response => {
			if (response.status === 200){
				resolve(response.data);
			}else{
				reject(response);
			}
		}).catch(e => {
			reject(e);
		});
	});
}

const deleteTechnology = (id: number) => {
	return new Promise<void>(async (resolve, reject) => {
		axios.delete(baseUrl + "technology/" + id).then(response => {
			if (response.status === 200){
				resolve();
			}else{
				reject();
			}
		}).catch(e => {
			reject(e);
		});
	})
}

const getTechnologyRingPlacementHistory = (technologyId: number, { expand }: { expand?: TechnologyRingPlacementExpandType } = {}) => {
	return new Promise<RingPlacementHistory[] | RingPlacementHistoryExpanded[]>(async (resolve, reject) => {
		let url = `${baseUrl}technology/${technologyId}/history`;
		if (expand !== undefined){
			const expandString = Object.entries(expand)
									.filter(([key, value]) => value)
									.map(([key, value]) => key)
									.join(',');

			url += `?expand=${expandString}`;
		}

		axios.get(url).then(response => {
			if (response.status === 200){
				resolve(response.data);
			}else{
				reject(response);
			}
		}).catch(e => {
			reject(e);
		});
	});
}

export { getTechnologies, createTechnology, updateTechnology, deleteTechnology, publishTechnology, getTechnologyRingPlacementHistory };