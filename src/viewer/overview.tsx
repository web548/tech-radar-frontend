import React, { useState, useEffect } from 'react';
import { AppBar, Toolbar, Typography, Container, Grid } from '@mui/material';
import { TechnologyExpanded, Category } from '../types';
import TechnologyViewerCategoryCard from './overview.card';

import { getTechnologies } from '../api/technology';
import { getCategories } from '../api/category';

function TechnologyViewerPage() {
	const [ technologies, setTechnologies ] = useState<TechnologyExpanded[]>([]);
	const [ categories, setCategories ] = useState<Category[]>([]);

	useEffect(() => {
		Promise.all([
			getTechnologies({ 
				published: true, 
				expand: {
					ring: true,
					category: true
				} 
			}), 
			getCategories()
		]).then(([ newTechnologies, categories ]) => {
			setTechnologies(newTechnologies as TechnologyExpanded[]);
			setCategories(categories);
		})
	}, []);

	return (
		<React.Fragment>
			<AppBar position="static">
				<Toolbar>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						Technologies
					</Typography>
				</Toolbar>
			</AppBar>
			<Container sx={{ mt: 2 }}>
				<Grid container spacing={2}>
					{categories.map(category => {
						const categoryTechnologies = technologies.filter(tech => tech.categoryId === category.id).sort((first, second) => (second.ringId as number - (first.ringId as number)));

						return (
							<Grid key={category.id} item xs={12} md={6}>
								<TechnologyViewerCategoryCard key={category.id} category={category.name} technologies={categoryTechnologies} />
							</Grid>
						);
					})}
				</Grid>
			</Container>
		</React.Fragment>
	);
}

export default TechnologyViewerPage;
