import TechnologyViewerPage from './overview';
import TechnologyViewerHistoryPage from './history';

export {
	TechnologyViewerPage,
	TechnologyViewerHistoryPage
}