import React from 'react';
import { useNavigate } from "react-router-dom";
import { Paper, Card, CardHeader, CardContent } from '@mui/material';
import { TableContainer, Table, TableBody, TableHead, TableRow, TableCell } from '@mui/material';
import { TechnologyExpanded } from '../types';

function TechnologyViewerCategoryCard(props: { category: string, technologies: TechnologyExpanded[] }) {
	const navigate = useNavigate();

	const { category, technologies } = props;

	const handleTechnologyClick = (technology: TechnologyExpanded) => {
		navigate("/viewer/history", { replace: false, state: { technology } });
	}

	return (
		<Card>
			<CardHeader
				title={category}
			/>
			<CardContent>
				<TableContainer component={Paper} sx={{ maxHeight: 300 }}>
					<Table stickyHeader size="small" aria-label={`${category.toLowerCase()} table`}>
						<TableHead>
							<TableRow>
								<TableCell>Technology</TableCell>
								<TableCell>Ring</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{technologies.map((technology) => (
								<TableRow hover key={technology.id} onClick={() => handleTechnologyClick(technology)}>
									<TableCell>{technology.name}</TableCell>
									<TableCell>{technology.ring?.name}</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</CardContent>
		</Card>
	);
}

export default TechnologyViewerCategoryCard;
