import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from "react-router-dom";
import { AppBar, Toolbar, Typography, Container, IconButton } from '@mui/material';
import { Card, CardHeader, CardContent, Avatar } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import HistoryIcon from '@mui/icons-material/History';
import { TechnologyExpanded, RingPlacementHistoryExpanded } from '../types';
import Moment from 'react-moment';

import { getTechnologyRingPlacementHistory } from '../api/technology';

type LocationState = {
	technology: TechnologyExpanded
}

function TechnologyViewerHistoryPage() {
	const navigate = useNavigate();
	const location = useLocation();

	// deep copy, so original state doesn't change
	const [ technology, setTechnology ] = useState<TechnologyExpanded>(JSON.parse(JSON.stringify((location.state as LocationState).technology)));
	const [ technologyRingPlacementHistory, setTechnologyRingPlacementHistory ] = useState<RingPlacementHistoryExpanded[]>([]);

	useEffect(() => {
		getTechnologyRingPlacementHistory(
			technology.id,
			{
				expand: {
					oldRing: true,
					newRing: true
				} 
			}
		).then((historyItems) => {
			setTechnologyRingPlacementHistory(historyItems as RingPlacementHistoryExpanded[]);
		})
	}, []);

	const handleBackClick = () => {
		navigate(-1);
	}

	return (
		<React.Fragment>
			<AppBar position="static">
				<Toolbar>
					<IconButton
						size="large"
						edge="start"
						aria-label="back"
						sx={{ mr: 2 }}
						onClick={handleBackClick}
					>
						<ArrowBackIcon />
					</IconButton>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						Ring placement history
					</Typography>
				</Toolbar>
			</AppBar>
			<Container sx={{ mt: 2 }}>
				<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
					{technology.name}
				</Typography>

				<Card sx={{ mt: 2 }}>
					<CardHeader
						avatar={
							<Avatar>
								<HistoryIcon />
							</Avatar>
						}
						title={technology.ring?.name}
								subheader={
									<Moment format="MMM DD, YYYY">
										{technology.modifiedAt}
									</Moment>
								}
					/>
					<CardContent>{technology.ringPlacementDescription}</CardContent>
				</Card>

				{
					technologyRingPlacementHistory.map((historyItem) => (
						<Card sx={{ mt: 2 }}>
							<CardHeader
								avatar={
									<Avatar>
										<HistoryIcon />
									</Avatar>
								}
								title={historyItem.oldRing?.name}
								subheader={
									<Moment format="MMM DD, YYYY">
										{historyItem.modifiedAt}
									</Moment>
								}
							/>
							<CardContent>{historyItem.oldRingPlacementDescription}</CardContent>
						</Card>
					))
				}
			</Container>
		</React.Fragment>
	);
}

export default TechnologyViewerHistoryPage;
